import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton friedChickenButton;
    private JButton chineseDumplingButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton sushiButton;
    private JLabel sidelabel;
    private JTextPane receivedInfo;
    private JTextPane receivedprice;
    private JButton checkOutButton;

    int total =0;
    int tax =0;

    void order(String food, int price){

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?"+"\n"+"This is "+price+"yen"+"(Excluding tax)",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);


        if(confirmation == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(
                    null,
                    "Will you be eating here?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation2 == 0) {
                tax = (int)((double)price*1.10);
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + " " + price + "(" +tax+ ")" + "yen" + "\n");
                total += tax;
                receivedprice.setText("Total " + total + "yen");
                JOptionPane.showMessageDialog(null, "Thank you ordering " + food + "!" + " It will be served as soon as possible.");
            }
            if (confirmation2 == 1) {
                tax = (int)((double)price*1.08);
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + " " + price + "(" +tax+ ")" + "yen" + "\n");
                total += tax;
                receivedprice.setText("Total " + total + "yen");
                JOptionPane.showMessageDialog(null, "Thank you ordering " + food + "!" + " It will be served as soon as possible.");
            }
        }
    }


    public FoodMachine() {
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("R.jpg")));
        friedChickenButton.setIcon(new ImageIcon(this.getClass().getResource("PAK85_syokutakubkaraage20150203185651.jpg")));
        chineseDumplingButton.setIcon(new ImageIcon(this.getClass().getResource("0adpDSC_6890-.jpg")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("R (1).jpg")));
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("R (2).jpg")));
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("adpDSC_7056-760x507-1.jpg")));

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 800);
            }
        });

        friedChickenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Chicken", 500);
            }
        });

        chineseDumplingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chinese Dumpling", 300);
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 600);
            }
        });

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 350);
            }
        });

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sushi", 1200);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ total + "yen");
                    receivedInfo.setText("\n");
                    total = 0;
                    receivedprice.setText("Total " + total + "yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodMachine");
        frame.setContentPane(new FoodMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
